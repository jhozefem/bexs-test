# Bexs Test

## Prerequisite

* [Docker](https://www.docker.com/community-edition)
* [Docker Compose](https://docs.docker.com/compose/install)

## Up and running

First.

```console
$ make build
```

Start application and set Input File:

```console
$ make up input-file={input-file-path}
```

For example:

```console
$ make up input-file=/Users/jhozefem/Desktop/input.csv
```

Check containers are successfully up and running:

```console
$ curl http://localhost:3000/health/check
```

Make sure you stop containers after usage:

```console
$ make stop
```

## Testing

Unit + Integration tests:

```console
$ make test
```

## Console command

```console
$ make lowest-route
```

## API

### Get all routes

```console
$ curl http://localhost:3000/v1/routes
```

Example:

```
Request:
GET http://localhost:3000/v1/routes

Response:
{
    "GRU": {
        "destinations": {
            "CDG": {
                "cost": 10
            },
            "BRC": {
                "cost": 5
            },
            "SCL": {
                "cost": 3
            }
        }
    },
    "SCL": {
        "destinations": {
            "CDG": {
                "cost": 8
            }
        }
    }
}
```

### Create a route

```console
$ curl --location --request POST 'http://localhost:3000/v1/routes' \
--header 'Content-Type: application/json' \
--data-raw '{
    "origin": "{origin}",
    "destination": "{destination}",
    "cost": {cost}
}'
```

Example:

```
Request:
POST http://localhost:3000/v1/routes
{
    "origin": "GRU",
    "destination": "BRC",
    "cost": 5
}

Response:
{
    "GRU": {
        "destinations": {
            "CDG": {
                "cost": 10
            },
            "BRC": {
                "cost": 5
            }
        }
    }
}
```

### Get lowest route

```console
$ curl http://localhost:3000/v1/routes/lowest/{origin}/{destination}
```

Example:

```
Request:
GET http://localhost:3000/v1/routes/lowest/GRU/CDG

Response:
{
    "cost": 10,
    "bestRoute": [
        "GRU",
        "CDG"
    ]
}
```