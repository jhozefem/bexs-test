const express = require("express")
const bodyParser = require("body-parser")
const HealthCheckController = require("./src/controllers/HealthCheckController")
const GetRoutesController = require("./src/controllers/v1/GetRoutesController")
const CreateRouteController = require("./src/controllers/v1/CreateRouteController")
const GetLowestRouteController = require("./src/controllers/v1/GetLowestRouteController")
const CreateRouteBodyValidatorInterceptor = require("./src/interceptors/CreateRouteBodyValidatorInterceptor")
const Log = require("./src/support/Log")

const app = express()
app.use(bodyParser.json())

app.use((error, req, res, next) => {
    Log.error(error.stack)
    res.status(500)
    res.send("Internal Server Error")
    return
})

/**
 * Health Check endpoint
*/
app.get("/health/check", (req, res) => {
    (new HealthCheckController).check(req, res)
})

/**
 * Get all routes endpoint
 */
app.get("/v1/routes", (req, res, next) => {
    (new GetRoutesController).get(req, res, next)
})

/**
 * Create a route endpoint
 *
 * Request Body:
 * {
 *     "origin": "{origin}",
 *     "destination": "{destination}",
 *     "cost": {cost}
 * }
 */
app.post("/v1/routes", (req, res, next) => {
    (new CreateRouteBodyValidatorInterceptor).intercept(req, res, next)
}, (req, res, next) => {
    (new CreateRouteController).create(req, res, next)
})

/**
 * Get lowest route (by origin and destination) endpoint
 */
app.get("/v1/routes/lowest/:origin/:destination", (req, res, next) => {
    (new GetLowestRouteController).get(req, res, next)
})

app.listen(3000, () => {
    Log.info("Application listening on port 3000!")
})