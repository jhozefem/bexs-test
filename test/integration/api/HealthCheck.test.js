"use strict"

const axios = require("axios")
const chai = require("chai")
const expect = chai.expect

describe("Integration/HealthCheck", () => {
    it("Should return success", () => {
        const request = {
            method: "GET",
            url: "http://localhost:3000/health/check"
        }

        return axios(request)
            .then(response => {
                expect(response.status).to.equal(200)
                expect(response.data).to.equal("success")
            })
    })
})