"use strict"

const axios = require("axios")
const chai = require("chai")
const expect = chai.expect

const fs = require("fs")

describe("Integration/GetLowestRoute", () => {
    describe("Lowest route found", () => {
        it("Should find a lowest route with one valid route in input", () => {
            const origin = "GRU"
            const destination = "CDG"

            let request

            fs.writeFileSync(
                process.env.DOCKER_INPUT_FILE,
                `GRU,CDG,10`
            )

            request = {
                method: "GET",
                url: `http://localhost:3000/v1/routes/lowest/${origin}/${destination}`
            }

            return axios(request)
                .then(response => {
                    expect(response.status).to.equal(200)
                    expect(response.data).to.eql({
                        cost: 10,
                        bestRoute: [
                            "GRU",
                            "CDG"
                        ]
                    })
                })
        })
    })

    describe("Lowest route not found", () => {
        it("Should throws an exception when hasn't a valid route", () => {
            const origin = "GRU"
            const destination = "CDG"

            let request

            fs.writeFileSync(
                process.env.DOCKER_INPUT_FILE,
                `GRU,BRC,10`
            )

            request = {
                method: "GET",
                url: `http://localhost:3000/v1/routes/lowest/${origin}/${destination}`
            }

            return axios(request)
                .then(() => {
                    // test error
                    expect(true).to.equal(false)
                })
                .catch(error => {
                    expect(error.response.status).to.equal(400)
                    expect(error.response.data).to.eql({
                        code: "BadRequestError",
                        message: "Lowest route not found"
                    })
                })
        })
    })
})