"use strict"

const axios = require("axios")
const chai = require("chai")
const expect = chai.expect

const fs = require("fs")

describe("Integration/GetRoutes", () => {
    it("Should return routes", () => {
        const request = {
            method: "GET",
            url: "http://localhost:3000/v1/routes"
        }

        const routes = {
            "GRU": {
                destinations: {
                    "BRC": {
                        cost: 10
                    }
                }
            },
            "BRC": {
                destinations: {
                    "SCL": {
                        cost: 5
                    }
                }
            }
        }

        fs.writeFileSync(
            process.env.DOCKER_INPUT_FILE,
            `GRU,BRC,10
BRC,SCL,5`
        )

        return axios(request)
            .then(response => {
                expect(response.status).to.equal(200)
                expect(response.data).to.eql(routes)
            })
    })

    it("Should return empty routes", () => {
        const request = {
            method: "GET",
            url: "http://localhost:3000/v1/routes"
        }

        const routes = {}

        fs.writeFileSync(
            process.env.DOCKER_INPUT_FILE,
            ""
        )

        return axios(request)
            .then(response => {
                expect(response.status).to.equal(200)
                expect(response.data).to.eql(routes)
            })
    })
})