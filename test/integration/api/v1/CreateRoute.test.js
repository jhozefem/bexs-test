"use strict"

const axios = require("axios")
const chai = require("chai")
const expect = chai.expect

const fs = require("fs")
const ErrorEnum = require("../../../../src/enum/ErrorEnum")

describe("Integration/CreateRoute", () => {
    describe("Validate", () => {
        describe("Field not defined", () => {
            it("Should get filled error list when origin is not defined", () => {
                const request = {
                    method: "POST",
                    url: "http://localhost:3000/v1/routes",
                    data: {
                        destination: "CDG",
                        cost: 10.50
                    }
                }

                return axios(request)
                    .then(() => {
                        // test error
                        expect(true).to.equal(false)
                    })
                    .catch(error => {
                        expect(error.response.status).to.equal(400)
                        expect(error.response.data).to.eql({
                            code: ErrorEnum.BAD_REQUEST_ERROR,
                            message: "One or more fields are invalid",
                            errors: [ "origin is not defined" ]
                        })
                    })
            })

            it("Should get filled error list when destination is not defined", () => {
                const request = {
                    method: "POST",
                    url: "http://localhost:3000/v1/routes",
                    data: {
                        origin: "GRU",
                        cost: 10.50
                    }
                }

                return axios(request)
                    .then(() => {
                        // test error
                        expect(true).to.equal(false)
                    })
                    .catch(error => {
                        expect(error.response.status).to.equal(400)
                        expect(error.response.data).to.eql({
                            code: ErrorEnum.BAD_REQUEST_ERROR,
                            message: "One or more fields are invalid",
                            errors: [ "destination is not defined" ]
                        })
                    })
            })

            it("Should get filled error list when cost is not defined", () => {
                const request = {
                    method: "POST",
                    url: "http://localhost:3000/v1/routes",
                    data: {
                        origin: "GRU",
                        destination: "CDG"
                    }
                }

                return axios(request)
                    .then(() => {
                        // test error
                        expect(true).to.equal(false)
                    })
                    .catch(error => {
                        expect(error.response.status).to.equal(400)
                        expect(error.response.data).to.eql({
                            code: ErrorEnum.BAD_REQUEST_ERROR,
                            message: "One or more fields are invalid",
                            errors: [ "cost is not defined" ]
                        })
                    })
            })
        })

        describe("Invalid type", () => {
            it("Should get filled error list when origin has a invalid type", () => {
                const request = {
                    method: "POST",
                    url: "http://localhost:3000/v1/routes",
                    data: {
                        origin: 1,
                        destination: "CDG",
                        cost: 10.50
                    }
                }

                return axios(request)
                    .then(() => {
                        // test error
                        expect(true).to.equal(false)
                    })
                    .catch(error => {
                        expect(error.response.status).to.equal(400)
                        expect(error.response.data).to.eql({
                            code: ErrorEnum.BAD_REQUEST_ERROR,
                            message: "One or more fields are invalid",
                            errors: [ "origin must be of type string" ]
                        })
                    })
            })

            it("Should get filled error list when destination has a invalid type", () => {
                const request = {
                    method: "POST",
                    url: "http://localhost:3000/v1/routes",
                    data: {
                        origin: "GRU",
                        destination: 1,
                        cost: 10.50
                    }
                }

                return axios(request)
                    .then(() => {
                        // test error
                        expect(true).to.equal(false)
                    })
                    .catch(error => {
                        expect(error.response.status).to.equal(400)
                        expect(error.response.data).to.eql({
                            code: ErrorEnum.BAD_REQUEST_ERROR,
                            message: "One or more fields are invalid",
                            errors: [ "destination must be of type string" ]
                        })
                    })
            })

            it("Should get filled error list when cost has a invalid type", () => {
                const request = {
                    method: "POST",
                    url: "http://localhost:3000/v1/routes",
                    data: {
                        origin: "GRU",
                        destination: "CDG",
                        cost: "10.50"
                    }
                }

                return axios(request)
                    .then(() => {
                        // test error
                        expect(true).to.equal(false)
                    })
                    .catch(error => {
                        expect(error.response.status).to.equal(400)
                        expect(error.response.data).to.eql({
                            code: ErrorEnum.BAD_REQUEST_ERROR,
                            message: "One or more fields are invalid",
                            errors: [ "cost must be of type number" ]
                        })
                    })
            })
        })
    })

    describe("Create", () => {
        it("Should create a route", () => {
            const request = {
                method: "POST",
                url: "http://localhost:3000/v1/routes",
                data: {
                    origin: "BRC",
                    destination: "SCL",
                    cost: 5
                }
            }

            const routes = {
                "GRU": {
                    destinations: {
                        "BRC": {
                            cost: 10
                        }
                    }
                },
                "BRC": {
                    destinations: {
                        "SCL": {
                            cost: 5
                        }
                    }
                }
            }

            fs.writeFileSync(
                process.env.DOCKER_INPUT_FILE,
                "GRU,BRC,10"
            )

            return axios(request)
                .then(response => {
                    expect(response.status).to.equal(201)
                    expect(response.data).to.eql(routes)
                })
        })
    })
})