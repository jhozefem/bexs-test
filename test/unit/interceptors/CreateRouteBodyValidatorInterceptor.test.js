"use strict"

const chai = require("chai")
const expect = chai.expect
const sinon = require('sinon')

const CreateRouteBodyValidatorInterceptor = require("../../../src/interceptors/CreateRouteBodyValidatorInterceptor")
const ErrorEnum = require("../../../src/enum/ErrorEnum")

describe("Unit/CreateRouteBodyValidatorInterceptor", () => {
    describe("Valid body", () => {
        it("Should get empty error list when request body is valid", () => {
            const req = {
                body: {
                    origin: "GRU",
                    destination: "CDG",
                    cost: 10.50
                }
            }
            const res = {}
            const next = sinon.spy()
            const interceptor = new CreateRouteBodyValidatorInterceptor

            interceptor.intercept(req, res, next)
            expect(next.called).to.equal(true)
        })
    })

    describe("Invalid body", () => {
        it("Should get filled error list when request body is empty", () => {
            const req = {
                body: null
            }
            const res = {
                status: sinon.spy(),
                send: sinon.spy()
            }
            const next = sinon.spy()
            const response = {
                code: ErrorEnum.BAD_REQUEST_ERROR,
                message: "One or more fields are invalid",
                errors: [ "body is empty" ]
            }
            const interceptor = new CreateRouteBodyValidatorInterceptor

            interceptor.intercept(req, res, next)
            expect(next.called).to.equal(false)
            expect(res.status.calledWithExactly(400)).to.equal(true)
            expect(res.send.calledWithExactly(response)).to.equal(true)
        })

        describe("Field not defined", () => {
            it("Should get filled error list when origin is not defined", () => {
                const req = {
                    body: {
                        destination: "CDG",
                        cost: 10.50
                    }
                }
                const res = {
                    status: sinon.spy(),
                    send: sinon.spy()
                }
                const next = sinon.spy()
                const response = {
                    code: ErrorEnum.BAD_REQUEST_ERROR,
                    message: "One or more fields are invalid",
                    errors: [ "origin is not defined" ]
                }
                const interceptor = new CreateRouteBodyValidatorInterceptor

                interceptor.intercept(req, res, next)
                expect(next.called).to.equal(false)
                expect(res.status.calledWithExactly(400)).to.equal(true)
                expect(res.send.calledWithExactly(response)).to.equal(true)
            })

            it("Should get filled error list when destination is not defined", () => {
                const req = {
                    body: {
                        origin: "GRU",
                        cost: 10.50
                    }
                }
                const res = {
                    status: sinon.spy(),
                    send: sinon.spy()
                }
                const next = sinon.spy()
                const response = {
                    code: ErrorEnum.BAD_REQUEST_ERROR,
                    message: "One or more fields are invalid",
                    errors: [ "destination is not defined" ]
                }
                const interceptor = new CreateRouteBodyValidatorInterceptor

                interceptor.intercept(req, res, next)
                expect(next.called).to.equal(false)
                expect(res.status.calledWithExactly(400)).to.equal(true)
                expect(res.send.calledWithExactly(response)).to.equal(true)
            })

            it("Should get filled error list when cost is not defined", () => {
                const req = {
                    body: {
                        origin: "GRU",
                        destination: "CDG"
                    }
                }
                const res = {
                    status: sinon.spy(),
                    send: sinon.spy()
                }
                const next = sinon.spy()
                const response = {
                    code: ErrorEnum.BAD_REQUEST_ERROR,
                    message: "One or more fields are invalid",
                    errors: [ "cost is not defined" ]
                }
                const interceptor = new CreateRouteBodyValidatorInterceptor

                interceptor.intercept(req, res, next)
                expect(next.called).to.equal(false)
                expect(res.status.calledWithExactly(400)).to.equal(true)
                expect(res.send.calledWithExactly(response)).to.equal(true)
            })
        })

        describe("Invalid type", () => {
            it("Should get filled error list when origin has a invalid type", () => {
                const req = {
                    body: {
                        origin: 1,
                        destination: "CDG",
                        cost: 10.50
                    }
                }
                const res = {
                    status: sinon.spy(),
                    send: sinon.spy()
                }
                const next = sinon.spy()
                const response = {
                    code: ErrorEnum.BAD_REQUEST_ERROR,
                    message: "One or more fields are invalid",
                    errors: [ "origin must be of type string" ]
                }
                const interceptor = new CreateRouteBodyValidatorInterceptor

                interceptor.intercept(req, res, next)
                expect(next.called).to.equal(false)
                expect(res.status.calledWithExactly(400)).to.equal(true)
                expect(res.send.calledWithExactly(response)).to.equal(true)
            })

            it("Should get filled error list when destination has a invalid type", () => {
                const req = {
                    body: {
                        origin: "GRU",
                        destination: 1,
                        cost: 10.50
                    }
                }
                const res = {
                    status: sinon.spy(),
                    send: sinon.spy()
                }
                const next = sinon.spy()
                const response = {
                    code: ErrorEnum.BAD_REQUEST_ERROR,
                    message: "One or more fields are invalid",
                    errors: [ "destination must be of type string" ]
                }
                const interceptor = new CreateRouteBodyValidatorInterceptor

                interceptor.intercept(req, res, next)
                expect(next.called).to.equal(false)
                expect(res.status.calledWithExactly(400)).to.equal(true)
                expect(res.send.calledWithExactly(response)).to.equal(true)
            })

            it("Should get filled error list when cost has a invalid type", () => {
                const req = {
                    body: {
                        origin: "GRU",
                        destination: "CDG",
                        cost: "10.50"
                    }
                }
                const res = {
                    status: sinon.spy(),
                    send: sinon.spy()
                }
                const next = sinon.spy()
                const response = {
                    code: ErrorEnum.BAD_REQUEST_ERROR,
                    message: "One or more fields are invalid",
                    errors: [ "cost must be of type number" ]
                }
                const interceptor = new CreateRouteBodyValidatorInterceptor

                interceptor.intercept(req, res, next)
                expect(next.called).to.equal(false)
                expect(res.status.calledWithExactly(400)).to.equal(true)
                expect(res.send.calledWithExactly(response)).to.equal(true)
            })
        })
    })
})