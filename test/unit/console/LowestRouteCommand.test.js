"use strict"

const chai = require("chai")
const expect = chai.expect
const proxyquire = require("proxyquire").noCallThru()

const LowestRouteCommandInputException = require("../../../src/exceptions/LowestRouteCommandInputException")

let LowestRouteCommand,
    readlineMock

describe("Unit/LowestRouteCommand", () => {
    before(() => {
        readlineMock = {
            createInterface: () => {
                return {
                    question: () => {}
                }
            }
        }

        LowestRouteCommand = proxyquire("../../../src/console/LowestRouteCommand", {
            "readline": readlineMock
        })
    })

    describe("GetInputOriginAndDestination", () => {
        it("Should get origin and destination when input is valid", () => {
            let response

            response = LowestRouteCommand.getInputOriginAndDestination("GRU-CDG")
            expect(response.origin).to.equal("GRU")
            expect(response.destination).to.equal("CDG")
        })

        it("Shouldn't get origin and destination when input is invalid", () => {
            expect(() => LowestRouteCommand.getInputOriginAndDestination("GRUCDG"))
                .to.throw(LowestRouteCommandInputException)
        })
    })
})