"use strict"

const chai = require("chai")
const expect = chai.expect
const proxyquire = require("proxyquire").noCallThru()

const LowestRouteNotFoundException = require("../../../src/exceptions/LowestRouteNotFoundException")

let RouteServiceMock,
    LowestRouteService

describe("Unit/LowestRouteService", () => {
    before(() => {
        RouteServiceMock = {}

        LowestRouteService = proxyquire("../../../src/services/LowestRouteService", {
            "../services/RouteService": RouteServiceMock
        })
    })

    beforeEach(() => {
        RouteServiceMock.getAll = () => {}
    })

    describe("Lowest route found", () => {
        it("Should find a lowest route with one valid route in input", () => {
            const origin = "GRU"
            const destination = "CDG"

            const routes = {
                "GRU": {
                    destinations: {
                        "CDG": {
                            cost: 10
                        }
                    }
                }
            }

            let response

            RouteServiceMock.getAll = () => routes
            response = (new LowestRouteService).get(origin, destination)

            expect(response.getCost()).to.equal(10)
            expect(response.getBestRoute()).to.eql([ "GRU", "CDG" ])
        })

        it("Should find a lowest route with two valid routes in input", () => {
            const origin = "GRU"
            const destination = "CDG"

            const routes = {
                "GRU": {
                    destinations: {
                        "CDG": {
                            cost: 10
                        },
                        "BRC": {
                            cost: 5
                        }
                    }
                },
                "BRC": {
                    destinations: {
                        "CDG": {
                            cost: 4
                        }
                    }
                }
            }

            let response

            RouteServiceMock.getAll = () => routes
            response = (new LowestRouteService).get(origin, destination)

            expect(response.getCost()).to.equal(9)
            expect(response.getBestRoute()).to.eql([ "GRU", "BRC", "CDG" ])
        })

        it("Should find a lowest route with three valid routes in input", () => {
            const origin = "GRU"
            const destination = "CDG"

            const routes = {
                "GRU": {
                    destinations: {
                        "CDG": {
                            cost: 10
                        },
                        "BRC": {
                            cost: 5
                        },
                        "SCL": {
                            cost: 5.5
                        }
                    }
                },
                "BRC": {
                    destinations: {
                        "CDG": {
                            cost: 4
                        }
                    }
                },
                "SCL": {
                    destinations: {
                        "BRC": {
                            cost: 4
                        },
                        "CDG": {
                            cost: 3
                        }
                    }
                }
            }

            let response

            RouteServiceMock.getAll = () => routes
            response = (new LowestRouteService).get(origin, destination)

            expect(response.getCost()).to.equal(8.5)
            expect(response.getBestRoute()).to.eql([ "GRU", "SCL", "CDG" ])
        })

        it("Should find a lowest route when routes has cycles", () => {
            const origin = "GRU"
            const destination = "CDG"

            const routes = {
                "GRU": {
                    destinations: {
                        "SCL": {
                            cost: 7
                        },
                        "BRC": {
                            cost: 5.5
                        },
                        "CDG": {
                            cost: 100
                        }
                    }
                },
                "BRC": {
                    destinations: {
                        "SCL": {
                            cost: 3.5
                        }
                    }
                },
                "SCL": {
                    destinations: {
                        "GRU": {
                            cost: 1
                        },
                        "BRC": {
                            cost: 2
                        }
                    }
                }
            }

            let response

            RouteServiceMock.getAll = () => routes
            response = (new LowestRouteService).get(origin, destination)

            expect(response.getCost()).to.equal(100)
            expect(response.getBestRoute()).to.eql([ "GRU", "CDG" ])
        })
    })

    describe("Lowest route not found", () => {
        it("Should throws an exception when hasn't a valid route", () => {
            const origin = "GRU"
            const destination = "CDG"

            const routes = {
                "GRU": {
                    destinations: {
                        "BRC": {
                            cost: 10
                        }
                    }
                }
            }

            RouteServiceMock.getAll = () => routes

            expect(() => (new LowestRouteService).get(origin, destination))
                .to.throw(LowestRouteNotFoundException)
        })

        it("Should throws an exception when origin is equals to destination", () => {
            const origin = "GRU"
            const destination = "GRU"

            const routes = {
                "GRU": {
                    destinations: {
                        "BRC": {
                            cost: 10
                        }
                    }
                }
            }

            RouteServiceMock.getAll = () => routes

            expect(() => (new LowestRouteService).get(origin, destination))
                .to.throw(LowestRouteNotFoundException)
        })
    })
})