"use strict"

const chai = require("chai")
const expect = chai.expect
const sinon = require('sinon')
const proxyquire = require("proxyquire").noCallThru()

let fsMock,
    RouteService

describe("Unit/RouteService", () => {
    before(() => {
        fsMock = {}

        RouteService = proxyquire("../../../src/services/RouteService", {
            "fs": fsMock
        })
    })

    beforeEach(() => {
        fsMock.writeFileSync = sinon.spy()
        fsMock.readFileSync = () => {}
    })

    describe("Create", () => {
        it("Should't insert new line when file is empty", () => {
            const inputFile = ""

            fsMock.readFileSync = () => inputFile

            RouteService.create("BRC", "SCL", 5)
            expect(fsMock.writeFileSync.calledWithExactly(process.env.DOCKER_INPUT_FILE, "BRC,SCL,5")).to.equal(true)
        })

        it("Should insert new line when file isn't empty", () => {
            const inputFile = "GRU,BRC,10"

            fsMock.readFileSync = () => inputFile

            RouteService.create("BRC", "SCL", 5)
            expect(fsMock.writeFileSync.calledWithExactly(process.env.DOCKER_INPUT_FILE, `GRU,BRC,10
BRC,SCL,5`)).to.equal(true)
        })
    })

    describe("GetAll", () => {
        it("Should get routes when file has one valid line", () => {
            const inputFile = "GRU,BRC,10"

            const routes = {
                "GRU": {
                    destinations: {
                        "BRC": {
                            cost: 10
                        }
                    }
                }
            }

            fsMock.readFileSync = () => inputFile
            expect(RouteService.getAll()).to.eql(routes)
        })

        it("Should get routes when file has five valid lines", () => {
            const inputFile = `GRU,BRC,10
BRC,SCL,5
GRU,CDG,75
GRU,SCL,20
GRU,ORL,56`

            const routes = {
                "GRU": {
                    destinations: {
                        "BRC": {
                            cost: 10
                        },
                        "CDG": {
                            cost: 75
                        },
                        "SCL": {
                            cost: 20
                        },
                        "ORL": {
                            cost: 56
                        }
                    }
                },
                "BRC": {
                    destinations: {
                        "SCL": {
                            cost: 5
                        }
                    }
                }
            }

            fsMock.readFileSync = () => inputFile
            expect(RouteService.getAll()).to.eql(routes)
        })

        it("Should get routes when file has five valid lines and one invalid", () => {
            const inputFile = `GRU,BRC,10
BRC,SCL,5
GRU,CDG,75
GRU,SCL,20
GRU,ORL,56
ORL;CDG;5`

            const routes = {
                "GRU": {
                    destinations: {
                        "BRC": {
                            cost: 10
                        },
                        "CDG": {
                            cost: 75
                        },
                        "SCL": {
                            cost: 20
                        },
                        "ORL": {
                            cost: 56
                        }
                    }
                },
                "BRC": {
                    destinations: {
                        "SCL": {
                            cost: 5
                        }
                    }
                }
            }

            fsMock.readFileSync = () => inputFile
            expect(RouteService.getAll()).to.eql(routes)
        })

        it("Should get empty routes when file hasn't lines", () => {
            const inputFile = ``

            const routes = {}

            fsMock.readFileSync = () => inputFile
            expect(RouteService.getAll()).to.eql(routes)
        })
    })
})