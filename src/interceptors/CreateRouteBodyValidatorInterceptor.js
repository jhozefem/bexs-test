"use strict"

const ErrorEnum = require("../enum/ErrorEnum")

const notDefinedError = field => `${field} is not defined`
const typeStringError = field => `${field} must be of type string`
const typeNumberError = field => `${field} must be of type number`

/**
 * Request body validator of create a route endpoint
 */
class CreateRouteBodyValidatorInterceptor
{
    intercept(req, res, next) {
        const errors = []

        if (!req.body) {
            errors.push("body is empty")
        } else {
            /**
             * origin field validation
             */
            if (req.body.origin === undefined || req.body.origin === null) {
                errors.push(notDefinedError("origin"))
            } else if (typeof req.body.origin != "string") {
                errors.push(typeStringError("origin"))
            }

            /**
             * destination field validation
             */
            if (req.body.destination === undefined || req.body.destination === null) {
                errors.push(notDefinedError("destination"))
            } else if (typeof req.body.destination != "string") {
                errors.push(typeStringError("destination"))
            }

            /**
             * cost field validation
             */
            if (req.body.cost === undefined || req.body.cost === null) {
                errors.push(notDefinedError("cost"))
            } else if (typeof req.body.cost != "number") {
                errors.push(typeNumberError("cost"))
            }
        }

        if (errors.length > 0) {
            res.status(400)
            res.send({
                code: ErrorEnum.BAD_REQUEST_ERROR,
                message: "One or more fields are invalid",
                errors
            })

            return
        }

        return next()
    }
}

module.exports = CreateRouteBodyValidatorInterceptor