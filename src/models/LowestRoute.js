"use strict"

class LowestRoute
{
    constructor(route) {
        this.route = route
        this.bestRoute = []

        return this
    }

    getCost() {
        return this.route.getCost()
    }

    getBestRoute() {
        let current = this.route
        this.bestRoute = []

        while (current != null) {
            this.bestRoute.push(current.getCity())
            current = current.getParentRoute()
        }

        this.bestRoute = this.bestRoute.reverse()

        return this.bestRoute
    }

    toJson() {
        return {
            cost: this.getCost(),
            bestRoute: this.getBestRoute()
        }
    }

    toString() {
        let output = null

        this.getBestRoute().forEach(city => {
            if (!output) {
                output = city
            } else {
                output = `${output} - ${city}`
            }
        })

        return `${output} > $${this.getCost()}`
    }
}

module.exports = LowestRoute