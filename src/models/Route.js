"use strict"

class Route
{
    constructor(city) {
        this.city = city
        this.parentRoute = null
        this.cost = 0
        this.routes = []
    }

    getCity() {
        return this.city
    }

    setParentRoute(parentRoute) {
        this.parentRoute = parentRoute
        return this
    }

    getParentRoute() {
        return this.parentRoute
    }

    setCost(cost) {
        this.cost = cost
        return this
    }

    getCost() {
        return this.cost
    }

    addRoute(route) {
        route.setParentRoute(this)
        this.routes.push(route)
        return this
    }

    getRoutes() {
        return this.routes
    }
}

module.exports = Route