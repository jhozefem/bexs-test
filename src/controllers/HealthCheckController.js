"use strict"

class HealthCheckController
{
    check(req, res) {
        res.status(200)
        res.send("success")
        return
    }
}

module.exports = HealthCheckController