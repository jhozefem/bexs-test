"use strict"

const RouteService = require("../../services/RouteService")

class CreateRouteController
{
    create(req, res, next) {
        const origin = req.body.origin
        const destination = req.body.destination
        const cost = req.body.cost
        let routes

        try {
            RouteService.create(origin, destination, cost)
            routes = RouteService.getAll()
        } catch (err) {
            res.status(500)
            res.send({
                code: ErrorEnum.INTERNAL_SERVER_ERROR
            })
            return
        }

        res.status(201)
        res.send(routes)
        return
    }
}

module.exports = CreateRouteController