"use strict"

const LowestRouteService = require("../../services/LowestRouteService")
const LowestRouteNotFoundException = require("../../exceptions/LowestRouteNotFoundException")
const ErrorEnum = require("../../enum/ErrorEnum")

class GetLowestRouteController
{
    get(req, res, next) {
        let lowestRoute

        try {
            lowestRoute = (new LowestRouteService)
                            .get(req.params.origin, req.params.destination)
        } catch (err) {
            if (err instanceof LowestRouteNotFoundException) {
                res.status(400)
                res.send({
                    code: ErrorEnum.BAD_REQUEST_ERROR,
                    message: err.message
                })
                return
            }

            res.status(500)
            res.send({
                code: ErrorEnum.INTERNAL_SERVER_ERROR
            })
            return
        }

        res.status(200)
        res.send(lowestRoute.toJson())
        return
    }
}

module.exports = GetLowestRouteController