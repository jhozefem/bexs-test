"use strict"

const RouteService = require("../../services/RouteService")

class GetRoutesController
{
    get(req, res, next) {
        let routes

        try {
            routes = RouteService.getAll()
        } catch (err) {
            res.status(500)
            res.send({
                code: ErrorEnum.INTERNAL_SERVER_ERROR
            })
            return
        }

        res.status(200)
        res.send(routes)
        return
    }
}

module.exports = GetRoutesController