"use strict"

module.exports = {
    BAD_REQUEST_ERROR: "BadRequestError",
    INTERNAL_SERVER_ERROR: "InternalServerError"
}