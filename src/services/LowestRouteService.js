"use strict"

const Route = require("../models/Route")
const LowestRoute = require("../models/LowestRoute")
const LowestRouteNotFoundException = require("../exceptions/LowestRouteNotFoundException")
const RouteService = require("../services/RouteService")

let lowestRoute, routes

const calculate = (origin, destination, current = null, cost = 0, accumulator = []) => {
    let route

    if (current == null) {
        current = origin
    }

    route = new Route(current)
    route.setCost(cost)

    if (current == destination) {
        if (lowestRoute == null || lowestRoute.getCost() > route.getCost()) {
            lowestRoute = new LowestRoute(route)
        }

        return route
    }

    if (!routes[current] || accumulator.includes(current)) {
        return null
    }

    accumulator.push(current)

    for (const dest in routes[current].destinations) {
        const cost = routes[current].destinations[dest].cost + route.getCost()
        let result = calculate(origin, destination, dest, cost, accumulator)

        if (result) {
            route.addRoute(result)
        }
    }

    if (route.getRoutes().length == 0) {
        return null
    }

    return route
}

class LowestRouteService
{
    get(origin, destination) {
        let response

        if (origin == destination) {
            throw new LowestRouteNotFoundException
        }

        lowestRoute = null
        routes = RouteService.getAll()
        response = calculate(origin, destination)

        if (!response) {
            throw new LowestRouteNotFoundException
        }

        return lowestRoute
    }
}

module.exports = LowestRouteService