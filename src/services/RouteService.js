"use strict"

const fs = require("fs")

const getInputFileContent = () => fs.readFileSync(process.env.DOCKER_INPUT_FILE, "utf8")

class RouteService
{
    create(origin, destination, cost) {
        let fileContent = getInputFileContent()

        if (fileContent != "") {
            fileContent += "\n"
        }

        fs.writeFileSync(
            process.env.DOCKER_INPUT_FILE,
            `${fileContent}${origin},${destination},${cost}`
        )
    }

    getAll() {
        const routes = {}
        const fileContent = getInputFileContent()
        const fileLines = fileContent.split("\n")

        /**
         * Line:
         * {origin},{destination},{cost}
         *
         * Transform to structure:
         * {
         *     {origin} => {
         *         destinations: {
         *             {destination}: {
         *                 cost: {cost}
         *             }
         *         }
         *     }
         * }
         */
        fileLines.forEach(line => {
            const matches = line.match(/^([\w]+),([\w]+),([\d\.]+)$/)

            if (matches) {
                const origin = matches[1]
                const destination = matches[2]
                const cost = matches[3]

                if (!routes[origin]) {
                    routes[origin] = {
                        destinations: {}
                    }
                }

                routes[origin].destinations[destination] = {
                    cost: parseFloat(cost, 10)
                }
            }
        })

        return routes
    }
}

module.exports = new RouteService