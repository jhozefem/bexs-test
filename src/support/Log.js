"use strict"

const bunyan = require("bunyan")
const LogLevelEnum = require("../enum/LogLevelEnum")

class Log {
    constructor() {
        this.streams = [{
            stream: process.stdout,
            level: LogLevelEnum.INFO
        }]

        this.log = bunyan.createLogger({
            streams: this.streams,
            name: "bexs-app",
            serializers: {
                req: bunyan.stdSerializers.req,
                res: bunyan.stdSerializers.res,
                error: bunyan.stdSerializers.err
            }
        })
    }

    info(message) {
        this.log.info("[INFO] " + message)
    }

    error(message) {
        this.log.error("[ERROR] " + message)
    }
}

module.exports = new Log