"use strict"

const MESSAGE = "Invalid input: origin and destination are required"

class LowestRouteCommandInputException extends Error {
    constructor() {
        super(MESSAGE)
        this.name = this.constructor.name
    }
}

module.exports = LowestRouteCommandInputException