"use strict"

const MESSAGE = "Lowest route not found"

class LowestRouteNotFoundException extends Error {
    constructor() {
        super(MESSAGE)
        this.name = this.constructor.name
    }
}

module.exports = LowestRouteNotFoundException