"use strict"

const LowestRouteService = require("../services/LowestRouteService")
const LowestRouteCommandInputException = require("../exceptions/LowestRouteCommandInputException")
const readline = require("readline")

class LowestRouteCommand
{
    constructor() {
        this.readLine = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })

        this.execute()
    }

    getInputOriginAndDestination(input) {
        const matches = input.match(/^([\w]+)\-([\w]+)$/)

        if (!matches || !matches[1] || !matches[2]) {
            throw new LowestRouteCommandInputException
        }

        return {
            origin: matches[1],
            destination: matches[2]
        }
    }

    execute() {
        this.readLine.question("please enter the route: ", route => {
            try {
                let originAndDestination,
                    lowestRoute

                originAndDestination = this.getInputOriginAndDestination(route)

                lowestRoute = (new LowestRouteService)
                                .get(originAndDestination.origin, originAndDestination.destination)

                console.log(`best route: ${lowestRoute.toString()}`)
            } catch (err) {
                console.error(`[ERROR] ${err.message}`)
            }

            this.execute()
        })
    }
}

module.exports = new LowestRouteCommand