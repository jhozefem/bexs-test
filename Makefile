.PHONY: build
build:
	npm install
	LOCAL_INPUT_FILE="" docker-compose build

.PHONY: up
up:
	LOCAL_INPUT_FILE=${input-file} docker-compose up -d

.PHONY: stop
stop:
	docker-compose stop

.PHONY: down
down:
	docker-compose down

.PHONY: attach
attach:
	docker-compose exec bexs-app bash

.PHONY: attach-test
attach-test:
	docker-compose exec bexs-app-test bash

.PHONY: logs
logs:
	docker-compose logs -f

.PHONY: test
test:
	docker-compose exec bexs-app-test npm test --silent

.PHONY:
lowest-route:
	docker-compose exec bexs-app node src/console/LowestRouteCommand