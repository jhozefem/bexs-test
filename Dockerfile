FROM node:12.13.1-alpine

ENV OS_PACKAGES  "bash openssh git python make"
RUN apk add ${OS_PACKAGES} --update --no-cache && \
    rm -rf /var/cache/apk

WORKDIR /opt/bexs-app
EXPOSE 3000

ENTRYPOINT ["npm", "start"]